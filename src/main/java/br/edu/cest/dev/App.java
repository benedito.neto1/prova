package br.edu.cest.dev;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Livro livro1 = new Livro();
       livro1.setAutor("Paulinho Moska");
       livro1.setEdicao("4º");
       livro1.setVolume("3");
       
       Item item1 = new Item();
       item1.setEditora("Moderna");
       item1.setTitulo("As aventuras de Sofia");
       item1.setAnoPublicacao("2016");
       item1.setIsbn("9780374530716");
       item1.setPreco("R$100,00");
       
       item1.display();
       livro1.display();
       
       ParImpar numero = new ParImpar();
       numero.retorno();
       
    		   
    }
}
